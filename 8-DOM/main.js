//Создайте страницу с кнопкой, при нажатии по кнопке на странице создается div с произвольнім тестом. 
//После создания 10 дивов все они должны удалится 

//ПЕРВЫЙ ВАРИАНТ

// const createDiv = () => {
//     const body = document.querySelector("body"),
//     parentDiv = document.querySelector("div"),
//     item = document.createElement("div"),
//     divCount = document.querySelectorAll("div > div");
//     item.innerHTML = "NEW DIV";
//         if (divCount.length < 10) {
//         parentDiv.appendChild(item);
//         } else {
//         body.removeChild(parentDiv);
//         }
//     }

//ВТОРОЙ ВАРИАНТ (мне кажеться более правильный)


    const createDiv = () => {
        const parentDiv = document.querySelector("div"),
        item = document.createElement("div"),
        divCount = document.querySelectorAll("div > div");
        if (divCount.length < 10) {
            item.innerHTML = "NEW DIV";
            parentDiv.appendChild(item);
        } else {
            divCount.forEach(element =>{
                parentDiv.removeChild(element);
            })
        }
        
        }
        